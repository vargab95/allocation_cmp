#include <stdio.h>
#include <string.h>
#include <time.h>
#include <m_libs/m_map.h>

int main(int argc, char **argv)
{
    clock_t t;
    double time_taken_system = 0;
    double time_taken_arena = 0;
    double time_taken_fixed = 0;
    double time_taken_slab = 0;
    m_alloc_instance_t *allocator;
    m_map_t *map;
    const int no_items = atoi(argv[1]);

    t = clock();
    allocator = m_alloc_create((m_alloc_config_t){.type = M_ALLOC_TYPE_SYSTEM}).allocator;
    map = m_map_create(allocator, 10000);
    for (int i = 0; i < no_items; i++)
    {
        m_com_sized_data_t tmp;

        tmp.size = sizeof(i);
        tmp.data = &i;

        m_map_store(map, &tmp, &tmp);
    }
    m_map_destroy(&map);
    m_alloc_destroy(&allocator);
    time_taken_system = (clock() - (double)t) / CLOCKS_PER_SEC;
    printf("System: %lf s\n", time_taken_system);

    t = clock();
    allocator = m_alloc_create((m_alloc_config_t){
        .type = M_ALLOC_TYPE_FIXED,
        .u = {
            .fixed = {
                .minimum_size = 268435456 // 256 MB
            }
        }
    }).allocator;
    map = m_map_create(allocator, 10000);
    for (int i = 0; i < no_items; i++)
    {
        m_com_sized_data_t tmp;

        tmp.size = sizeof(i);
        tmp.data = &i;

        m_map_store(map, &tmp, &tmp);
    }
    m_alloc_destroy(&allocator);
    time_taken_fixed = (clock() - (double)t) / CLOCKS_PER_SEC;
    printf("Fixed:  %lf s (%03.2f %% - %03.2f %%)\n",
            time_taken_fixed,
            time_taken_fixed / time_taken_system * 100,
            time_taken_system / time_taken_fixed * 100);

    t = clock();
    allocator = m_alloc_create((m_alloc_config_t){
        .type = M_ALLOC_TYPE_ARENA,
        .u = {
            .arena = {
                .minimum_size_per_arena = 8096
            }
        }
    }).allocator;
    map = m_map_create(allocator, 10000);
    for (int i = 0; i < no_items; i++)
    {
        m_com_sized_data_t tmp;

        tmp.size = sizeof(i);
        tmp.data = &i;

        m_map_store(map, &tmp, &tmp);
    }
    m_alloc_destroy(&allocator);
    time_taken_arena = (clock() - (double)t) / CLOCKS_PER_SEC;
    printf("Arena:  %lf s (%03.2f %% - %03.2f %%)\n",
            time_taken_arena,
            time_taken_arena / time_taken_system * 100,
            time_taken_system / time_taken_arena * 100);

#ifdef USE_SLAB
    t = clock();
    size_t cell_sizes[] = { 16, 64, 512, 1024, 2048, 4096, 1048576, 0 };
    allocator = m_alloc_create((m_alloc_config_t){
        .type = M_ALLOC_TYPE_SLAB,
        .u = {
            .slab = {
                .minimum_no_cells_per_arena = 128,
                .cell_sizes = cell_sizes
            }
        }
    }).allocator;
    map = m_map_create(allocator, 10000);
    for (int i = 0; i < no_items; i++)
    {
        m_com_sized_data_t tmp;

        tmp.size = sizeof(i);
        tmp.data = &i;

        m_map_store(map, &tmp, &tmp);
    }
    m_alloc_destroy(&allocator);
    time_taken_arena = (clock() - (double)t) / CLOCKS_PER_SEC;
    printf("Slab:  %lf s (%03.2f %% - %03.2f %%)\n",
            time_taken_slab,
            time_taken_slab / time_taken_system * 100,
            time_taken_system / time_taken_slab * 100);
#endif
}
