#include <stdio.h>
#include <string.h>
#include <time.h>
#include <m_libs/m_map.h>

int load_dictionary(m_map_t *map, const char *path)
{
    char buffer[65536];
    char *buffer_ptr = buffer;
    size_t buffer_size = sizeof(buffer) / sizeof(buffer[0]);

    FILE *dictionary = fopen(path, "r");
    if (!dictionary)
    {
        fprintf(stderr, "Failed to open %s\n", path);
        return -1;
    }

    int32_t no_read_bytes;
    while ((no_read_bytes = getline(&buffer_ptr, &buffer_size, dictionary)) > 0)
    {
        m_com_sized_data_t key;
        m_com_sized_data_t value;
        size_t no_key_bytes;
        char *separator;

        if (buffer[0] == '#')
        {
            continue;
        }

        separator = strstr(buffer, "::");

        key.data = buffer_ptr;
        key.size = separator - buffer;

        value.data = separator + 2;
        value.size = no_read_bytes - key.size - 2;

        if (!m_map_store(map, &key, &value))
        {
            fprintf(stderr, "Failed to add buffer %s\n", buffer);
            break;
        }
    }

    fclose(dictionary);

    return 0;
}

int main(int argc, char **argv)
{
    clock_t t;
    double time_taken_system = 0;
    double time_taken_arena = 0;
    double time_taken_fixed = 0;
    double time_taken_slab = 0;
    m_alloc_instance_t *allocator;
    m_map_t *map;

    t = clock();
    allocator = m_alloc_create((m_alloc_config_t){.type = M_ALLOC_TYPE_SYSTEM}).allocator;
    map = m_map_create(allocator, 10000);
    load_dictionary(map, argv[1]);
    m_map_destroy(&map);
    m_alloc_destroy(&allocator);
    time_taken_system = (clock() - (double)t) / CLOCKS_PER_SEC;
    printf("System: %lf s\n", time_taken_system);

    t = clock();
    allocator = m_alloc_create((m_alloc_config_t){
        .type = M_ALLOC_TYPE_FIXED,
        .u = {
            .fixed = {
                .minimum_size = 67108864 // 64 MB
            }
        }
    }).allocator;
    map = m_map_create(allocator, 10000);
    load_dictionary(map, argv[1]);
    m_alloc_destroy(&allocator);
    time_taken_fixed = (clock() - (double)t) / CLOCKS_PER_SEC;
    printf("Fixed:  %lf s (%03.2f %% - %03.2f %%)\n",
            time_taken_fixed,
            time_taken_fixed / time_taken_system * 100,
            time_taken_system / time_taken_fixed * 100);

    t = clock();
    allocator = m_alloc_create((m_alloc_config_t){
        .type = M_ALLOC_TYPE_ARENA,
        .u = {
            .arena = {
                .minimum_size_per_arena = 8096
            }
        }
    }).allocator;
    map = m_map_create(allocator, 10000);
    load_dictionary(map, argv[1]);
    m_alloc_destroy(&allocator);
    time_taken_arena = (clock() - (double)t) / CLOCKS_PER_SEC;
    printf("Arena:  %lf s (%03.2f %% - %03.2f %%)\n",
            time_taken_arena,
            time_taken_arena / time_taken_system * 100,
            time_taken_system / time_taken_arena * 100);

#ifdef USE_SLAB
    t = clock();
    size_t cell_sizes[] = { 16, 64, 512, 1024, 2048, 4096, 0 };
    allocator = m_alloc_create((m_alloc_config_t){
        .type = M_ALLOC_TYPE_SLAB,
        .u = {
            .slab = {
                .minimum_no_cells_per_arena = 1024,
                .cell_sizes = cell_sizes
            }
        }
    }).allocator;
    map = m_map_create(allocator, 10000);
    load_dictionary(map, argv[1]);
    m_alloc_destroy(&allocator);
    time_taken_slab = (clock() - (double)t) / CLOCKS_PER_SEC;
    printf("Slab:  %lf s (%03.2f %% - %03.2f %%)\n",
            time_taken_slab,
            time_taken_slab / time_taken_system * 100,
            time_taken_system / time_taken_slab * 100);
#endif
}
